package me.powercas_gamer.essentials.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static me.powercas_gamer.essentials.utils.Color.style;
import static me.powercas_gamer.essentials.utils.Configuration.getConfig;

public class JoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if (getConfig().getBoolean("listener.join", true)) {
            e.setJoinMessage(style("&f" + p + " &ehas joined the server."));
        }
        if (getConfig().getBoolean("listener.join", false)) {
            e.setJoinMessage(null);
        }
    }
}
