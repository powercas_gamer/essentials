package me.powercas_gamer.essentials.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static me.powercas_gamer.essentials.utils.Color.style;
import static me.powercas_gamer.essentials.utils.Configuration.getConfig;

public class FilterListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent  e) {
        Player p = e.getPlayer();
        String message = e.getMessage().toLowerCase();

        for (String blockedWord : getConfig().getStringList("filter")) {
            if (message.contains(blockedWord.toLowerCase())) {
                p.sendMessage(style("&cYour message has been cancelled because it has a filtered word in it."));
                e.setCancelled(true);
                break;
            }
        }
    }
}
