package me.powercas_gamer.essentials.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import static me.powercas_gamer.essentials.utils.Color.style;
import static me.powercas_gamer.essentials.utils.Configuration.getConfig;

public class QuitListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (getConfig().getBoolean("listener.quit", true)) {
            e.setQuitMessage(style("&f" + p + " &ehas left the server."));
        }
        if (getConfig().getBoolean("listener.quit", false)) {
            e.setQuitMessage(null);
        }
    }
}
