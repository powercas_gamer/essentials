package me.powercas_gamer.essentials.commands.other;

import com.destroystokyo.paper.Title;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.essentials.utils.Color.style;

public class BirthdayCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("essentials.command.birthday")) {
                sender.sendMessage(style("server.no_permission"));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/birthday [player]"));
            }

            //Player target = Bukkit.getPlayerExact(args[0]);
            Player target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                sender.sendMessage(style("&cPlayer not found or offline."));
            } else {
                player.sendMessage(style("&aDone..."));
                Bukkit.broadcastMessage("&8[&4&lALERT&8] &eIts &f" + args[0] + " &etheir birthday! Please congratulate him!");
                target.setDisplayName(style("&d&l" + target.getName()));
                target.setHealth(20);
                target.setFoodLevel(20);
                target.setSaturation(20);
            }
        }
        return true;
    }
}
