package me.powercas_gamer.essentials.commands.other;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.essentials.Essentials.getChat;
import static me.powercas_gamer.essentials.Essentials.getEconomy;
import static me.powercas_gamer.essentials.utils.Color.style;

public class WhoisCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("essentials.command.whois")) {
                sender.sendMessage(style("server.no_permission"));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/whois p[player]"));
            }

            //Player target = Bukkit.getPlayerExact(args[0]);
            Player target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                sender.sendMessage(style("&cPlayer not found or offline."));
            } else {
                try {

                    World world = target.getWorld();
                    String group = getChat().getPrimaryGroup(target);
                    String prefix = getChat().getPlayerPrefix(target);
                    String suffix = getChat().getPlayerSuffix(target);
                    double balance = getEconomy().getBalance(target);

                    sender.sendMessage(style("&7&m----------------------------------------------------"));
                    sender.sendMessage(style("&8[&f" + target.getDisplayName() + "&8]"));
                    sender.sendMessage(style("&7» &eUUID: &f" + target.getUniqueId()));
                    if (player.hasPermission("essentials.command.whois.ip")) {
                        sender.sendMessage(style("&7» &eIP: &f" + target.getAddress()));
                    }
                    sender.sendMessage(style("&7» &eRank: &f" + group));
                    sender.sendMessage(style("&7» &eOperator: &f" + target.isOp()));
                    sender.sendMessage(style("&7» &eBalance: &f" + balance));
                    sender.sendMessage(style("&7» &ePrefix: &f" + prefix) + "&f(" + prefix + "&f)");
                    sender.sendMessage(style("&7» &eSuffix: &f" + suffix) + "&f(" + suffix + "&f)");
                    sender.sendMessage(style("&7&m----------------------------------------------------"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }
}
