package me.powercas_gamer.essentials.commands.other;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.essentials.utils.Color.style;

public class ListCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("essentials.command.list")) {
                sender.sendMessage(style("server.no_permission"));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&7&m----------------------------------------------------"));
                sender.sendMessage(style("&eThere are currently &6" + Bukkit.getOnlinePlayers() + "&eonline out of &6" + Bukkit.getMaxPlayers() + "&e."));
                sender.sendMessage(style("&7&m----------------------------------------------------"));
            }
        }
        return true;
    }
}

