package me.powercas_gamer.essentials.commands.other;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static me.powercas_gamer.essentials.utils.Color.style;

public class KitCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("essentials.command.kit")) {
                sender.sendMessage(style("server.no_permission"));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/kit [kit]"));
            }
            if (alias.equalsIgnoreCase("starter")) {
                if (!player.hasPermission("essentials.command.kit.starter")) {
                    sender.sendMessage(style("&cYou don't have permission to use this kit. &7(&fessentials.command.kit.starter&7)"));
                }
                // items
                ItemStack sword = new ItemStack(Material.IRON_SWORD);
                ItemStack pickaxe = new ItemStack(Material.IRON_PICKAXE);
                ItemStack axe = new ItemStack(Material.IRON_AXE);
                ItemStack shovel = new ItemStack(Material.IRON_SHOVEL);
                ItemStack food = new ItemStack(Material.COOKED_PORKCHOP);
                food.setAmount(16);
                player.sendMessage(style("&eYou've been given the &6Starter &ekit."));
                player.getInventory().addItem(sword, pickaxe, axe, shovel, food);
            }

            if (alias.equalsIgnoreCase("food")) {
                if (!player.hasPermission("essentials.command.kit.food")) {
                    sender.sendMessage(style("&cYou don't have permission to use this kit. &7(&fessentials.command.kit.food&7)"));
                }
                // items
                ItemStack food = new ItemStack(Material.COOKED_PORKCHOP);
                food.setAmount(32);
                player.sendMessage(style("&eYou've been given the &6Food &ekit."));
                player.getInventory().addItem(food);
            }
        }
        return true;
    }
}
