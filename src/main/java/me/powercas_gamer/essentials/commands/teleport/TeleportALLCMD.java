package me.powercas_gamer.essentials.commands.teleport;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.essentials.utils.Color.style;
import static me.powercas_gamer.essentials.utils.Configuration.getConfig;

public class TeleportAllCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("essentials.command.teleportall")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
                return true;
            }
            for (Player o : Bukkit.getOnlinePlayers()) {
                if (o.getPlayer() != player) {
                    // Not needed but w/e
                    if (o == null) {
                        sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                    }
                    sender.sendMessage(style("&eTeleporting &6" + o + " &eto yourself."));
                    o.sendMessage(style("&eYou are being teleported to &6" + player.getName() + "&e."));
                    o.teleport(player);
                }
            }
        }
        return true;
    }
}




