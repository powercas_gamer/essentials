package me.powercas_gamer.essentials.commands.teleport;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.essentials.utils.Color.style;

public class TeleportCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("essentials.command.teleport")) {
                sender.sendMessage(style("server.no_permission"));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/teleport [player]"));
            }

            //Player target = Bukkit.getPlayerExact(args[0]);
            Player target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                sender.sendMessage(style("&cPlayer not found or offline."));
            }
            if (target == player) {
                sender.sendMessage(style("&cYou cannot teleport to yourself."));
            } else {
                player.teleport(target);
                player.sendMessage(style("&eTeleporting you to &6" + target.getName() + "&e."));
            }
        }
        return true;
    }
}
