package me.powercas_gamer.essentials.commands.player;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static me.powercas_gamer.essentials.utils.Color.style;

public class FlyCMD implements CommandExecutor {
    private List<Player> fly = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("essentials.command.fly")) {
                sender.sendMessage(style("server.no_permission"));
            }
            if (!fly.contains(player)) {
                player.sendMessage(style("&eYou have &6enabled &eyour fly mode."));
                player.setAllowFlight(true);
                fly.add(player);
            }
            if (fly.contains(player)) {
                player.sendMessage(style("&eYou have &6disabled &eyour fly mode."));
                player.setAllowFlight(false);
                fly.remove(player);
            }
            if (!player.hasPermission("essentials.command.fly.other")) {
                player.sendMessage(style("&cYou don't have permission to use this command. &7(&fessentials.command.fly.other&7)"));
            }
            // target
            Player target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
            }
            if (!fly.contains(target)) {
                target.sendMessage(style("&eYour fly mode has been &6enabled&e."));
                player.sendMessage(style("&eYou have &6enabled &f" + target.getName() + " &etheir fly mode."));
                target.setAllowFlight(true);
                fly.add(player);
            }
            if (fly.contains(target)) {
                target.sendMessage(style("&eYour fly mode has been &6disabled&e."));
                player.sendMessage(style("&eYou have &6disabled &f" + target.getName() + " &etheir fly mode."));
                target.setAllowFlight(false);
                fly.remove(player);
            }
        }
        return true;
    }
}

