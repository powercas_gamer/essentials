package me.powercas_gamer.essentials.commands.player;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.essentials.utils.Color.style;

public class GamemodeCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("essentials.command.gamemode")) {
                sender.sendMessage(style("server.no_permission"));
            }
            if (args.length < 1) {
                sender.sendMessage(style("usage"));
            }
            if (alias.equalsIgnoreCase("survival") || alias.equalsIgnoreCase("0") | alias.equalsIgnoreCase("s")) {
                player.sendMessage(style("&eYour gamemode has been set to &6Survival&e."));
                player.setGameMode(GameMode.SURVIVAL);
            }
            if (alias.equalsIgnoreCase("creative") || alias.equalsIgnoreCase("1") | alias.equalsIgnoreCase("c")) {
                player.sendMessage(style("&eYour gamemode has been set to &6Creative&e."));
                player.setGameMode(GameMode.CREATIVE);
            }
            if (alias.equalsIgnoreCase("adventure") || alias.equalsIgnoreCase("2") | alias.equalsIgnoreCase("a")) {
                player.sendMessage(style("&eYour gamemode has been set to &6Adventure&e."));
                player.setGameMode(GameMode.ADVENTURE);
            }
            if (alias.equalsIgnoreCase("spectator") || alias.equalsIgnoreCase("3") | alias.equalsIgnoreCase("sp")) {
                player.sendMessage(style("&eYour gamemode has been set to &6Spectator&e."));
                player.setGameMode(GameMode.SPECTATOR);
            }
            if (!player.hasPermission("essentials.command.fly.other")) {
                player.sendMessage(style("&cYou don't have permission to use this command. &7(&fessentials.command.gamemode.other&7)"));
            }
            // target
            Player target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
            }
            if (alias.equalsIgnoreCase("survival") || alias.equalsIgnoreCase("0") || alias.equalsIgnoreCase("s")) {
                player.sendMessage(style("&eYou have set &f" + target.getName() + " &etheir gamemode to &6Survival&e."));
                target.sendMessage(style("&eYour gamemode has been set to &6Survival&e."));
                target.setGameMode(GameMode.SURVIVAL);
            }
            if (alias.equalsIgnoreCase("creative") || alias.equalsIgnoreCase("1") || alias.equalsIgnoreCase("c")) {
                player.sendMessage(style("&eYou have set &f" + target.getName() + " &etheir gamemode to &6Creative&e."));
                target.sendMessage(style("&eYour gamemode has been set to &6Creative&e."));
                target.setGameMode(GameMode.CREATIVE);
            }
            if (alias.equalsIgnoreCase("adventure") || alias.equalsIgnoreCase("2") || alias.equalsIgnoreCase("a")) {
                player.sendMessage(style("&eYou have set &f" + target.getName() + " &etheir gamemode to &6Adventure&e."));
                target.sendMessage(style("&eYour gamemode has been set to &6Adventure&e."));
                target.setGameMode(GameMode.ADVENTURE);
            }
            if (alias.equalsIgnoreCase("spectator") || alias.equalsIgnoreCase("3") || alias.equalsIgnoreCase("sp")) {
                player.sendMessage(style("&eYou have set &f" + target.getName() + " &etheir gamemode to &6Spectator&e."));
                target.sendMessage(style("&eYour gamemode has been set to &6Spectator&e."));
                target.setGameMode(GameMode.SPECTATOR);
            }
        }
        return true;
    }
}