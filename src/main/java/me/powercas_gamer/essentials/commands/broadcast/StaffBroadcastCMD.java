package me.powercas_gamer.essentials.commands.broadcast;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.essentials.utils.Color.style;

public class StaffBroadcastCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("essentials.command.staffbroadcast")) {
                sender.sendMessage(style("server.no_permission"));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/broadcast <message>"));
            }
            String b = StringUtils.join(args, " ");
            for (Player o : Bukkit.getOnlinePlayers()) {
                if (o.hasPermission("essentials.command.staffbroadcast.see")) {
                    Bukkit.broadcastMessage(style("&8[&b&lSTAFF&8] &f" + b));
                }
            }
        }
        return true;
    }
}
