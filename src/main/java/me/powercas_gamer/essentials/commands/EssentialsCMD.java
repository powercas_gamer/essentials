package me.powercas_gamer.essentials.commands;

import me.powercas_gamer.essentials.Essentials;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.essentials.utils.Color.style;
import static me.powercas_gamer.essentials.utils.Configuration.getConfig;
import static org.bukkit.Bukkit.getServer;

public class EssentialsCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("essentials.admin")) {
                sender.sendMessage(style("server.no_permission"));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&7&m----------------------------------------------------"));
                sender.sendMessage(style(" "));
                sender.sendMessage(style("&6&lEssentials Commands."));
                sender.sendMessage(style(" "));
                sender.sendMessage(style("&7» &e/essentials version &8- &7Check what version you're using."));
                sender.sendMessage(style("&7» &e/essentials reload &8- &7Reload the essentials plugin."));
                sender.sendMessage(style("&7&m----------------------------------------------------"));
            }
            if (alias.equalsIgnoreCase("version")) {
                if (!(Bukkit.getPluginManager().isPluginEnabled("Vault"))) {
                    sender.sendMessage(style("&7&m----------------------------------------------------"));
                    sender.sendMessage(style("&6&lServer Version: &e" + getServer().getBukkitVersion() + "&7(&fMC: " + getServer().getVersion() + "&7)"));
                    sender.sendMessage(style("&6&lEssentials Version: &e" + getServer().getPluginManager().getPlugin(Essentials.getInstance().getName()).getDescription().getVersion()));
                    sender.sendMessage(style("&7&m----------------------------------------------------"));
                }
                if (Bukkit.getPluginManager().isPluginEnabled("Vault")) {
                    sender.sendMessage(style("&7&m----------------------------------------------------"));
                    sender.sendMessage(style("&6&lServer Version: &e" + getServer().getBukkitVersion() + "&7(&fMC: " + getServer().getVersion() + "&7)"));
                    sender.sendMessage(style("&6&lEssentials Version: &e" + getServer().getPluginManager().getPlugin(Essentials.getInstance().getName()).getDescription().getVersion()));
                    sender.sendMessage(style("&6&lVault Version: &e" + Bukkit.getPluginManager().getPlugin("Vault").getDescription().getVersion()));
                    sender.sendMessage(style("&7&m----------------------------------------------------"));
                }
            }
            if (alias.equalsIgnoreCase("reload")) {
                Essentials.getInstance().reloadConfig();
                getConfig().getString(style("plugin.reload").replaceAll("%prefix%", "plugin.prefix"));
            }
        }
        return true;
    }
}