package me.powercas_gamer.essentials.utils;

// Java Imports
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

// Bukkit Imports
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

// Random Imports
import jdk.nashorn.internal.runtime.regexp.joni.Config;


public class Configuration {

    // Variables
    private static YamlConfiguration Config;
    private static File configFile;
    private static boolean loaded = false;

    /**
     * Gets the configuration file.
     *
     * @return the Config
     */
    public static YamlConfiguration getConfig() {
        if (!loaded) {
            loadConfig();
        }
        return Config;
    }

    /**
     * Gets the configuration file.
     *
     * @return Configuration file
     */
    public static File getConfigFile() {
        return configFile;
    }

    /**
     * Loads the configuration file from the .jar.
     */
    public static void loadConfig() {
        configFile = new File(Bukkit.getServer().getPluginManager().getPlugin("Essentials").getDataFolder(), "config.yml");
        if (configFile.exists()) {
            Config = new YamlConfiguration();
            try {
                Config.load(configFile);
            } catch (FileNotFoundException ex) {
                // TODOa: throw (new FileNotFoundException ("[Core] An error has occured in the config file:", ex));
            } catch (IOException ex) {
                // TODaO: throw (new IOException ("[Core] An error has occured in the config file:", ex));
            } catch (InvalidConfigurationException ex) {
                // TODOa: Log exception
            }
            loaded = true;
        } else {
            try {
                Bukkit.getServer().getPluginManager().getPlugin("Essentials").getDataFolder().mkdir();
                InputStream jarURL = Config.class.getResourceAsStream("/config.yml");
                copyFile(jarURL, configFile);
                Config = new YamlConfiguration();
                Config.load(configFile);
                loaded = true;
                // IN MAIN CLASS
            } catch (Exception e) {
                // TODoy: throw (new Exception ("[Core] An error has occured in the config file.", e));
            }
        }
    }

    /**
     * Copies a file to a new location.
     *
     * @param in InputStream
     * @param out File
     *
     * @throws Exception
     */
    static private void copyFile(InputStream in, File out) throws Exception {
        InputStream fis = in;
        FileOutputStream fos = new FileOutputStream(out);
        try {
            byte[] buf = new byte[1024];
            int i = 0;
            while ((i = fis.read(buf)) != -1) {
                fos.write(buf, 0, i);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (fis != null) {
                fis.close();
            }
            if (fos != null) {
                fos.close();
            }
        }
    }

    /**
     * Constructor of SpaceConfig.
     *
     private Config() {
     }
     */
}