// Package Defintion
package me.powercas_gamer.essentials;

// Imports
import me.powercas_gamer.essentials.commands.EssentialsCMD;
import me.powercas_gamer.essentials.commands.broadcast.BroadcastCMD;
import me.powercas_gamer.essentials.commands.broadcast.RawBroadcastCMD;
import me.powercas_gamer.essentials.commands.broadcast.StaffBroadcastCMD;
import me.powercas_gamer.essentials.commands.other.BirthdayCMD;
import me.powercas_gamer.essentials.commands.other.KitCMD;
import me.powercas_gamer.essentials.commands.other.ListCMD;
import me.powercas_gamer.essentials.commands.other.WhoisCMD;
import me.powercas_gamer.essentials.commands.player.FlyCMD;
import me.powercas_gamer.essentials.commands.player.GamemodeCMD;
import me.powercas_gamer.essentials.commands.teleport.TeleportAllCMD;
import me.powercas_gamer.essentials.commands.teleport.TeleportCMD;
import me.powercas_gamer.essentials.commands.teleport.TeleportHereCMD;
import me.powercas_gamer.essentials.listeners.FilterListener;
import me.powercas_gamer.essentials.listeners.JoinListener;
import me.powercas_gamer.essentials.listeners.QuitListener;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;


// Class Definition
public final class  Essentials extends JavaPlugin {
    private static Essentials instance;
    private static Economy econ = null;
    private static Permission perms = null;
    private static Chat chat = null;

    @Override
    public void onEnable() {
        if (!setupEconomy() ) {
            System.out.println("[Essentials] Disabling plugin due to no vault depend found!");
            //getServer().getPluginManager().disablePlugin(this);
            return;
        }
        if (setupEconomy()) {
            System.out.println("[Essentials] Enabling plugin....");
            System.out.println("[Essentials] Registering Instance....");
            instance = this;
            System.out.println("[Essentials] Successfully registered instance!");
            System.out.println("[Essentials] Registering Commands....");
            setupCommands();
            System.out.println("[Essentials] Vault found! Hooking into Vault.");
            setupChat();
            setupPermissions();
            System.out.println("[Essentials] Registering Configuration file...");
            setupConfig();
            System.out.println("[Essentials] Successfully registered configuration files!");
            System.out.println("[Essentials] The plugin has successfully been enabled.");

        }
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static Permission getPermissions() {
        return perms;
    }

    public static Chat getChat() {
        return chat;
    }

    public static Economy getEconomy() {
        return econ;
    }

    private void setupCommands() {
        getCommand("whois").setExecutor(new WhoisCMD());
        getCommand("essentials").setExecutor(new EssentialsCMD());
        getCommand("birthday").setExecutor(new BirthdayCMD());
        getCommand("list").setExecutor(new ListCMD());
        //getCommand("spawn").setExecutor(new SpawnCMD());
        getCommand("teleportall").setExecutor(new TeleportAllCMD());
        getCommand("teleport").setExecutor(new TeleportCMD());
        getCommand("teleporthere").setExecutor(new TeleportHereCMD());
        getCommand("kit").setExecutor(new KitCMD());
        getCommand("broadcast").setExecutor(new BroadcastCMD());
        getCommand("rawbroadcast").setExecutor(new RawBroadcastCMD());
        getCommand("staffbroadcast").setExecutor(new StaffBroadcastCMD());
        getCommand("fly").setExecutor(new FlyCMD());
        getCommand("gamemode").setExecutor(new GamemodeCMD());
    }

    private void setupListeners() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(new JoinListener(), this);
        pm.registerEvents(new QuitListener(), this);
        pm.registerEvents(new FilterListener(), this);
    }

    private void setupConfig() {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdirs();
        }
        saveDefaultConfig();
        getConfig();
    }


    public static Essentials getInstance() {
        return instance;
    }



}
